.___  __________________  __________.__                        .___
|   |/  _____/\______   \ \______   \__| ____ _____ _______  __| _/
|   /   \  ___ |    |  _/  |     ___/  |/ ___\\__  \\_  __ \/ __ | 
|   \    \_\  \|    |   \  |    |   |  \  \___ / __ \|  | \/ /_/ | 
|___|\______  /|______  /  |____|   |__|\___  >____  /__|  \____ | 
            \/        \/                    \/     \/           \/ 

________________________________________________________________________________________________

This is a clone of the official picard svn repo available at 

svn://svn.code.sf.net/p/picard/code, but it also includes additional 
patches which have been made to accommodate the Integrate Genome Browser codebase. 

The need for these patches may not be permanent, but as long as the patches are in 
the place this repository will be the official build of the patched picard 
libraries for IGB. 

The repository can be synchronized with any future updates to picard's official svn 
repo using git-svn.
